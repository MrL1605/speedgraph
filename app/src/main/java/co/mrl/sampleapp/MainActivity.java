package co.mrl.sampleapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.hrules.charter.CharterBar;
import com.hrules.charter.CharterLine;
import com.hrules.charter.CharterXLabels;
import com.hrules.charter.CharterYLabels;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements LocationListener{

    public final static long EXECUTING_PERIOD_TIME = 5 * 1000;
    public final static long MINIMUM_DISTANCE = 100;
    public final static String TAG = "MainActivity LOG";
    LocationManager locationManager;
    private List<Float> values;
    CharterLine charterLineWithYLabel;
    CharterYLabels charterLineYLabel;
    CharterBar charterBarWithYLabel;
    CharterYLabels charterBarYLabel;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Resources res = getResources();
        int[] barColors = new int[] {
                res.getColor(R.color.colorAccent), res.getColor(R.color.colorAccent),
                res.getColor(R.color.colorAccent)
        };

        locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            onProviderDisabled("Permission Required");
            Log.i(TAG, "Permission Required");
            return;
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.i(TAG, "Network Enabled");
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i(TAG, "GPS Enabled");
                //locationManager.removeUpdates(this);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        EXECUTING_PERIOD_TIME, MINIMUM_DISTANCE, this);
            } else {
                onProviderDisabled("GPS not enabled");
            }
        } else {
            onProviderDisabled("Network Not enabled");
        }

        values = new ArrayList<>();
        values.add(0f);
        values.add(0f);
        float[] float_arr = new float[values.size()];
        String[] str_arr = new String[values.size()];
        for (int i=0; i<values.size(); i++){
            float_arr[i] = values.get(i);
            str_arr[i] = values.get(i) + "";
        }

        // charter_line_YLabel
        charterLineYLabel = (CharterYLabels) findViewById(R.id.charter_line_YLabel);
        charterLineYLabel.setValues(str_arr);

        charterLineWithYLabel = (CharterLine) findViewById(R.id.charter_line_with_YLabel);
        charterLineWithYLabel.setValues(float_arr);
        charterLineWithYLabel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

                float[] float_arr = new float[values.size()];
                String[] str_arr = new String[values.size()];
                for (int i=0; i<values.size(); i++){
                    float_arr[i] = values.get(i);
                    str_arr[i] = values.get(i) + "";
                }

                charterLineYLabel.setValues(str_arr);
                charterLineWithYLabel.setValues(float_arr);
                charterLineWithYLabel.show();
            }
        });


        // charter_bar_YLabel
        charterBarYLabel = (CharterYLabels) findViewById(R.id.charter_bar_YLabel);
        charterBarYLabel.setVisibilityPattern(new boolean[] { true, false });
        charterBarYLabel.setValues(str_arr);

        charterBarWithYLabel = (CharterBar) findViewById(R.id.charter_bar_with_YLabel);
        charterBarWithYLabel.setValues(float_arr);
        charterBarWithYLabel.setColors(barColors);
        charterBarWithYLabel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

                float[] float_arr = new float[values.size()];
                String[] str_arr = new String[values.size()];
                for (int i=0; i<values.size(); i++){
                    float_arr[i] = values.get(i);
                    str_arr[i] = values.get(i) + "";
                }

                charterBarYLabel.setValues(str_arr);
                charterBarWithYLabel.setValues(float_arr);
                charterBarWithYLabel.show();

            }
        });


    }

    @Override public void onLocationChanged(Location location) {

        Log.i(TAG,"location val = " + location.getSpeed());
        TextView logger_tv = (TextView)findViewById(R.id.logger_tv);
        String log = logger_tv.getText().toString();
        log += " - " + location.getSpeed();
        logger_tv.setText(log);

        values.add(location.getSpeed());

        float[] float_arr = new float[values.size()];
        String[] str_arr = new String[values.size()];
        for (int i=0; i<values.size(); i++){
            float_arr[i] = values.get(i);
            str_arr[i] = values.get(i) + "";
        }
        charterLineWithYLabel.setValues(float_arr);
        charterLineYLabel.setValues(str_arr);
        charterLineWithYLabel.show();

        charterBarWithYLabel.setValues(float_arr);
        charterBarYLabel.setValues(str_arr);
        charterBarWithYLabel.show();

    }

    @Override public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override public void onProviderEnabled(String provider) {
    }

    @Override public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        finish();
        Toast.makeText(getBaseContext(), "GPS is turned off !",
                Toast.LENGTH_LONG).show();

    }


}
